A proof of the divergence of the harmonic serie using Lean.

This was my first contribution to https://github.com/leanprover-community/mathlib/ but it got replaced by a more general result about p-series. I wanted to save it somewhere, so here it is !